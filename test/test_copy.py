#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import pytest
import s3sync.cli
from boto.s3.connection import Location
from boto.s3.key import Key
from pytest_bdd import given, scenarios, then, when

scenarios('copy.feature')


@given("I have an existing key <key>")
def create_key(context, s3, key):
    bucket = s3.create_bucket('test_bucket', location=Location.EU)
    k = Key(bucket)
    k.key = key
    k.set_contents_from_string("""
С горы скатившись, камень лег в долине.
Как он упал? никто не знает ныне -
Сорвался ль он с вершины сам собой,
Иль был низринут волею чужой?
Столетье за столетьем пронеслося:
Никто еще не разрешил вопроса.

15 января 1833; 2 апреля 1857
""")


@pytest.fixture
def cli(context):
    source = "test_bucket"
    dest = "test_bucket_dest"
    with open('test/s3sync.json') as config_file:
        config = json.load(config_file)
    return s3sync.cli.Cli(config, source, dest)


@when("I copy the key <key>")
def copy_key(context, cli, key):
    cli.copy(key)


@then("The destination bucket contains key <key>")
def check_destination_contains_key(context, s3, key):
    k1 = Key(s3.lookup('test_bucket'))
    k2 = Key(s3.lookup('test_bucket_dest'))
    k1.name = key
    k2.name = key
    c1 = k1.get_contents_as_string()
    c2 = k2.get_contents_as_string()
    assert c1 == c2
