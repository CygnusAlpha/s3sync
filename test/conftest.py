import boto
import boto.s3.connection
import pytest


def in_docker():
    """
        Returns: True if running in a docker container, else False
    """
    try:
        with open('/proc/1/cgroup', 'rt') as ifh:
            return 'docker' in ifh.read()
    except FileNotFoundError:
        return False


def pytest_bdd_apply_tag(tag, function):
    if tag == 'docker' and not in_docker():
        marker = pytest.mark.skip(reason="Need to run this test in docker environment. docker-compose run pytest")
        marker(function)
        return True
    else:
        # Fall back to pytest-bdd's default behavior
        return None


@pytest.fixture
def context():
    return {}


@pytest.fixture
def access_key():
    return 'x'


@pytest.fixture
def secret_key():
    return 'x'


@pytest.fixture
def port():
    return 80


@pytest.fixture
def host():
    return "s3"


@pytest.fixture
def s3(access_key, secret_key, host, port):
    return boto.connect_s3(
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        host=host,
        port=port,
        is_secure=False,
        calling_format=boto.s3.connection.OrdinaryCallingFormat(),
    )
