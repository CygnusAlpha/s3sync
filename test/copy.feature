@docker
Feature: Copy
    s3sync copying


Scenario Outline: Copy a single key from src to dest
    Given I have an existing key <key>
    When I copy the key <key>
    Then The destination bucket contains key <key>

   Examples:
    | key  |
    | abcd | 
