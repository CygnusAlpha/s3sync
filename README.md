s3sync
======

Sync files between two separate s3 (or s3 like) accounts with different credentials.

Usage
-----

```
Usage:
  s3sync [-c <cfg file>] copy <source> <dest> <key>
  s3sync [-c <cfg file>] copy_all <source> <dest>
  s3sync (-h | --help)
  s3sync --version
```

Where the source and dest account names correspond to sections in the config file.

eg.

`s3sync copy rados s3 /spindle/769904ae2bfe49c58b3e3da03239e4e8`


Configuration
-------------

s3cmd requires a config file in JSON that specifies the account names and their individual
configurations. This config can be specified with -c or else the default ./s3sync.json will be used.

For the example above:

~~~~
{
    "rados" : {
        "host" : "localhost",
        "port" : "10080",
        "bucket" : "s3",
        "access_key" : "x",
        "secret_key" : "y",
        "is_secure" : false
    },
    "s3" : {
        "host" : "localhost",
        "port" : "10080",
        "bucket" : "real_s3_bucket",
        "access_key" : "x",
        "secret_key" : "y",
        "is_secure" : false
    }
}
~~~~

Installation
------------

* python setup.py install

Development
-----------

* pip install -r requirements.txt
* python setup.py test
* python setup.py develop
