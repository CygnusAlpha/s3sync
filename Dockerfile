FROM python:3.6

MAINTAINER custard@cpan.org
ENV project=s3cmd

RUN apt-get update && \
    apt-get -y install apt-utils

RUN apt-get install -y --no-install-recommends git wget vim
RUN apt-get -y install python-pip

WORKDIR /${project}

ADD requirements.txt /${project}
RUN pip install --upgrade -r requirements.txt

ADD . /${project}
