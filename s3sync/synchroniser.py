"""
    Synchroniser
"""
import logging
import tempfile
from multiprocessing import Pool

from boto.s3.key import Key

LOG = logging.getLogger()


class Synchroniser():
    def __init__(self, source, dest, processes=4):
        self.source = source
        self.dest = dest
        self.processes = processes

    def copy(self, key_name):
        LOG.info("copy: {} from: {} to: {}".format(key_name, self.source.bucket, self.dest.bucket))
        tmp = tempfile.NamedTemporaryFile()
        LOG.debug("download: tmp.name={}".format(tmp.name))
        # Download
        key = self.source.get_bucket().get_key(key_name)
        key.get_contents_to_filename(tmp.name)
        # upload
        LOG.debug("upload: tmp.name={}".format(tmp.name))
        k = Key(self.dest.get_bucket())
        k.key = key_name
        k.set_contents_from_filename(tmp.name)
        LOG.debug("done: rm: tmp.name={}".format(tmp.name))
        tmp.close()

    def copy_all(self):
        LOG.info("copy all: from: {} to: {}".format(self.source.bucket, self.dest.bucket))
        pool = Pool(processes=self.processes)
        for key in self.source.get_bucket().list():
            pool.apply_async(Synchroniser.copy, (self, key.name))
        pool.close()
        pool.join()

        LOG.debug("copy all: done")
