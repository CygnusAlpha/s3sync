import boto
import boto.s3.connection


class Connection():
    def __init__(self, access_key, secret_key, bucket, host='localhost', port=80, is_secure=True):
        self.connection = boto.connect_s3(
            aws_access_key_id=access_key,
            aws_secret_access_key=secret_key,
            host=host,
            port=int(port),
            is_secure=is_secure,
            calling_format=boto.s3.connection.OrdinaryCallingFormat(),
        )
        self.bucket = bucket

    def get_bucket(self):
        """
            Return a Bucket object for the named bucket in this connection
        """
        self.connection.create_bucket(self.bucket)
        return self.connection.lookup(self.bucket)
