"""
s3sync.cli

Command line program for s3sync

Usage:
  s3sync [-c <cfg file>] copy <source> <dest> <key>
  s3sync [-c <cfg file>] copy_all <source> <dest>
  s3sync (-h | --help)
  s3sync --version

Options:
  -c=<file>     Config File.
  -h --help     Show this screen.
  --version     Show version.

"""

import json
import logging
import os

from docopt import docopt

from s3sync.connection import Connection
from s3sync.synchroniser import Synchroniser

logging.basicConfig(
    level=logging.INFO,
    format='%(relativeCreated)6d %(threadName)s %(message)s'
)
LOG = logging.getLogger()


class Cli():
    def __init__(self, config, source, dest):
        self.config = config
        source_connection = self._create_connection(source)
        dest_connection = self._create_connection(dest)
        self.sync = Synchroniser(source_connection, dest_connection)

    def copy(self, key):
        self.sync.copy(key)

    def copy_all(self):
        self.sync.copy_all()

    def _create_connection(self, account):
        config = self.config[account]
        return Connection(
            access_key=config['access_key'],
            secret_key=config['secret_key'],
            bucket=config['bucket'],
            host=config['host'],
            port=config['port'],
            is_secure=config['is_secure']
        )


def main():
    """
        Load config
        Instantiate Cli
    """
    here = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(here, 'version.txt')) as f:
        VERSION = f.read().strip()

    arguments = docopt(__doc__, version=VERSION)
    if not arguments['-c']:
        arguments['-c'] = './s3sync.json'
    LOG.debug(arguments)

    with open(arguments['-c']) as config_file:
        config = json.load(config_file)

    cli = Cli(config, arguments['<source>'], arguments['<dest>'])
    if arguments['copy']:
        cli.copy(arguments['<key>'])
    elif arguments['copy_all']:
        cli.copy_all()
